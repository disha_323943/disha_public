package spring.com.attra;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CommentCount {

	   @SuppressWarnings("resource")
	public static void main(String[] args) throws IOException,FileNotFoundException {
		  // BufferedReader br=new BufferedReader(new FileReader("helloworld.java"));
		   File f=new File("helloworld.txt");
		   FileReader fr=new FileReader(f);
		   BufferedReader b=new BufferedReader(fr);
		   
		//Scanner sc = new Scanner(new FileReader("helloworld"));
	      String input;
	      int single =0;
	      int multiLine = 0;
	      while((input=b.readLine())!=null) {
	    	  
	    	  

	         if (input.contains("/*")) {
	            multiLine ++;
	         }

	         if(input.contains("//")) {
	            single ++;
	         }
	      }

	      System.out.println("no. of single line comments :"+single);
	      System.out.println("no. of multiple line comments :"+multiLine);
	   }
	}